//
// Created by antifallobst on 10/4/22.
//

#ifndef LIBCOSMOS_COSMOS_H
#define LIBCOSMOS_COSMOS_H

#include <cosmos/stdtypes.h>
#include <cosmos/string.h>

// TODO: replace "stdarg.h" with own implementation of variadic parameters
#include <stdarg.h>

/* TODO
 * Implement these functions:
 *   - printf
 *   - malloc
 *   - free
 *   - file operations
 * */

void print  (string_t str);
void printf (string_t str, ...);
void log    (string_t str);

void exit   (int status);

#endif //LIBCOSMOS_COSMOS_H
