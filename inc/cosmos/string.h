//
// Created by antifallobst on 10/6/22.
//

#ifndef COSMOS_STRING_H
#define COSMOS_STRING_H

#include <cosmos/stdtypes.h>

typedef const char* string_t;

int     strlen              (string_t str);
bool    strcmp              (string_t a, string_t b);
void    decimal_to_string   (int64_t value, char* buffer);

#endif //COSMOS_STRING_H
