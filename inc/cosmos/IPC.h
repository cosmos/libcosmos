//
// Created by antifallobst on 10/4/22.
//

#ifndef LIBCOSMOS_IPC_H
#define LIBCOSMOS_IPC_H

#include <cosmos/stdtypes.h>
#include <cosmos/status.h>
#include <cosmos/module.h>

typedef enum {
    IPC_STD_DESC_NONE,
    IPC_STD_DESC_BROADCAST,
    IPC_STD_DESC_END
} ipc_standard_descriptors_E;

typedef enum {
    IPC_MSG_STATUS,
    IPC_MSG_SIGNAL,
    IPC_MSG_MESSAGE,
    IPC_MSG_MODCOM
} ipc_message_type_E;

typedef enum {
    IPC_CONN_RECV,
    IPC_CONN_SEND,
    IPC_CONN_BIDIRECTIONAL
} ipc_connection_mode_E;

typedef struct {
    pid_t                   sender_pid;
    ipc_message_type_E      type;
    uint16_t                special;        // used for module calls to set a type of modcall
    uint8_t*                buffer;
    uint64_t                buffer_len;
} ipc_message_T;

typedef struct {
    uint16_t                descriptor;
    pid_t                   pid;
    ipc_connection_mode_E   mode;
} ipc_connection_T;

// IPC connection functions
ipc_connection_T    ipc_connect             (pid_t pid, ipc_connection_mode_E mode);
ipc_connection_T    ipc_conn_wait           (ipc_connection_mode_E mode);
ipc_connection_T    ipc_conn_wait_pid       (pid_t pid, ipc_connection_mode_E mode);
bool                ipc_conn_validate       (ipc_connection_T* connection);
status_t            ipc_close               (ipc_connection_T* connection);

// IPC communication functions
status_t            ipc_send                (ipc_connection_T* connection, uint8_t* buffer, size_t len_buffer);
status_t            ipc_send_string         (ipc_connection_T* connection, string_t str);
status_t            ipc_send_signal         (ipc_connection_T* connection, int signal);
status_t            ipc_pool_send           ();
ipc_message_T       ipc_receive             (ipc_connection_T* connection);


#endif //LIBCOSMOS_IPC_H
