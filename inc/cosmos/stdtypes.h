//
// Created by antifallobst on 10/6/22.
//

#ifndef COSMOS_STDTYPES_H
#define COSMOS_STDTYPES_H

typedef     signed char    int8_t;
typedef   unsigned char   uint8_t;
typedef     signed short   int16_t;
typedef   unsigned short  uint16_t;
typedef     signed int     int32_t;
typedef   unsigned int    uint32_t;
typedef     signed long    int64_t;
typedef   unsigned long   uint64_t;

typedef     uint32_t        pid_t;
typedef     uint64_t        size_t;
typedef     uint64_t        address_t;

typedef   unsigned char    bool;
#define     true	       1
#define     false	       0


#define     NULL            (void*)0

#endif //COSMOS_STDTYPES_H
