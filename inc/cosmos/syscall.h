//
// Created by antifallobst on 10/4/22.
//

#ifndef LIBCOSMOS_SYSCALL_H
#define LIBCOSMOS_SYSCALL_H

#include <cosmos/stdtypes.h>
#include <cosmos/status.h>

typedef enum {
    SYSCALL_MISC_EXIT       = 0x0001,

    SYSCALL_FS_OPEN         = 0x0101,
    SYSCALL_FS_CLOSE        = 0x0102,
    SYSCALL_FS_CREATE       = 0x0103,
    SYSCALL_FS_DELETE       = 0x0104,
    SYSCALL_FS_READ         = 0x0105,
    SYSCALL_FS_WRITE        = 0x0106,
    SYSCALL_FS_STAT         = 0x0107,
    SYSCALL_FS_LENSTAT      = 0x0108,

    SYSCALL_MOD_LOAD        = 0x0201,
    SYSCALL_MOD_LOADSTD     = 0x0202,
    SYSCALL_MOD_UNLOAD      = 0x0203,
    SYSCALL_MOD_CREATE      = 0x0204,
    SYSCALL_MOD_CONFIG      = 0x0205,
    SYSCALL_MOD_CLEANUP     = 0x0206,
    SYSCALL_MOD_SETSTD      = 0x0207,
    SYSCALL_MOD_GET_PID     = 0x0208,

    SYSCALL_IPC_CONNECT     = 0x0301,
    SYSCALL_IPC_CLOSE       = 0x0302,
    SYSCALL_IPC_CONN_WAIT   = 0x0303,
    SYSCALL_IPC_SEND        = 0x0304,
    SYSCALL_IPC_RECEIVE     = 0x0305,

    SYSCALL_MEM_MAP_PAGES   = 0x0401,
    SYSCALL_MEM_UNMAP_PAGES = 0x0402
} syscalls_E;

extern status_t asm_syscall(int id, uint64_t arg1, uint64_t arg2, uint64_t arg3, uint64_t arg4);

#endif //LIBCOSMOS_SYSCALL_H
