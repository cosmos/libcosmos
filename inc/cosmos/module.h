//
// Created by antifallobst on 10/25/22.
//

#ifndef COSMOS_MODULE_H
#define COSMOS_MODULE_H

#include <cosmos/stdtypes.h>

typedef enum {
    MOD_INVALID,
    MOD_LOGGER,
    MOD_RENDERER,
    MOD_KEYBOARD,
    MOD_SHELL,
    MOD_PCI,
    MOD_AHCI,
    MOD_VFAT,
    MOD_EXT2,
    MOD_POWER,
    MOD_USB
} module_E;

typedef enum {
    MOD_LOGGER_LOG              // MODULE INPUT
} module_logger_type_E;

typedef enum {
    MOD_RENDERER_PRINT,         // MODULE INPUT
    MOD_RENDERER_SET_PIXEL,     // MODULE INPUT
    MOD_RENDERER_GET_PIXEL,     // MODULE OUTPUT
    MOD_RENDERER_SET_BUFFER,    // MODULE INPUT
    MOD_RENDERER_GET_BUFFER,    // MODULE OUTPUT
    MOD_RENDERER_LOAD_FONT      // MODULE INPUT
} module_renderer_type_E;

typedef enum {
    MOD_KEYBOARD_CHAR_OUT,      // MODULE OUTPUT
    MOD_KEYBOARD_CHAR_IN        // MODULE INPUT
} module_keyboard_type_E;

typedef enum {
    MOD_POWER_SHUTDOWN,         // MODULE COMMAND
    MOD_POWER_REBOOT            // MODULE COMMAND
} module_power_type_E;

pid_t mod_get_pid(module_E mod);

#endif //COSMOS_MODULE_H
