//
// Created by antifallobst on 10/6/22.
//

#ifndef LIBCOSMOS_STATUS_H
#define LIBCOSMOS_STATUS_H

#include <cosmos/stdtypes.h>
#include <cosmos/string.h>

typedef int8_t status_t;

#define NUM_STATUS_CODES                7

#define IS_STATUS_ERROR(s)              (s < 0)

#define STATUS_RESOURCE_BUSY            -6
#define STATUS_TIMEOUT                  -5
#define STATUS_RESOURCE_NOT_AVAILABLE   -4
#define STATUS_PERMISSION_DENIED        -3
#define STATUS_ERROR                    -2
#define STATUS_WARNING                  -1
#define STATUS_SUCCESS                   0


string_t status_get_string(status_t status);

#endif //LIBCOSMOS_STATUS_H
