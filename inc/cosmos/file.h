//
// Created by antifallobst on 10/4/22.
//

#ifndef LIBCOSMOS_FILE_H
#define LIBCOSMOS_FILE_H

#include <cosmos/stdtypes.h>
#include <cosmos/status.h>
#include <cosmos/string.h>

typedef enum {
    FILE_WRITE,
    FILE_READ
} file_mode_E;

typedef enum {
    FILE_DESC_NONE,
    FILE_DESC_STDOUT,
    FILE_DESC_STDLOG
}file_std_descriptors_E;

typedef struct {
    uint32_t    descriptor;
    file_mode_E mode;
    uint64_t    cursor;
} file_t;

file_t      f_open          (string_t path, file_mode_E mode);
status_t    f_close         (file_t* file);
status_t    f_delete        (file_t* file);
status_t    f_read          (file_t* file, void* buffer, uint64_t num);
status_t    f_read_at_pos   (file_t* file, void* buffer, uint64_t num, uint64_t pos);
status_t    f_write         (file_t* file, void* buffer, uint64_t num);
status_t    f_write_at_pos  (file_t* file, void* buffer, uint64_t num, uint64_t pos);

extern file_t* stdout;
extern file_t* stdlog;


//file_t G_stdout;
//file_t G_stdlog;

#endif //LIBCOSMOS_FILE_H
