INCLUDEDIR = inc
SRCDIR := src
OBJDIR := ../build/libcosmos/lib

# Toolchain
CC = gcc
ASM = nasm
LD = ld

# Flags
CFLAGS = -ffreestanding -fno-stack-protector -I$(INCLUDEDIR)
ASMFLAGS =
LDFLAGS = -Bsymbolic -nostdlib -shared -fno-stack-protector

ARCHITECTURE = x86-64

rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

SRC = $(call rwildcard,$(SRCDIR),*.c)
ASMSRC = $(call rwildcard,$(SRCDIR),*.asm)
OBJS = $(patsubst $(SRCDIR)/%.c, $(OBJDIR)/%.o, $(SRC))
OBJS += $(patsubst $(SRCDIR)/%.asm, $(OBJDIR)/%.o, $(ASMSRC))
DIRS = $(wildcard $(SRCDIR)/*)

libcosmos: setup $(OBJS) link copy

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@ echo !==== COMPILING $^
	@ mkdir -p $(@D)
	$(CC) $(CFLAGS) -c $^ -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.asm
	@ echo !==== ASSEMBLING $^
	@ mkdir -p $(@D)
	$(ASM) $(ASMFLAGS) $^ -f elf64 -o $@

link:
	@ echo !===== LINKING
	$(LD) $(LDFLAGS) -o ../build/libcosmos/libcosmos.so $(OBJS)

copy:
	cp ../build/libcosmos/libcosmos.so ../binaries/ramdisk/libcosmos.so

setup:
	mkdir -p ../build/
	mkdir -p ../build/libcosmos
	mkdir -p $(OBJDIR)

clean:
	rm -rf ../build/libcosmos