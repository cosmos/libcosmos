//
// Created by antifallobst on 10/4/22.
//
// This file is the holy THERMOMIX
//

#include <cosmos.h>
#include <cosmos/syscall.h>
#include <cosmos/file.h>

void print(string_t str) {
    f_write(stdout, (void*)str, strlen(str));
}


void log(string_t str) {
    f_write(stdlog, (void*)str, strlen(str));
}


void exit(int status) {
    asm_syscall(SYSCALL_MISC_EXIT, status, 0, 0, 0);
}