//
// Created by antifallobst on 10/4/22.
//

#include <cosmos/IPC.h>
#include <cosmos/syscall.h>
#include <cosmos/string.h>

bool ipc_conn_validate(ipc_connection_T* connection) {
    return (connection->descriptor >= IPC_STD_DESC_END);
}

ipc_connection_T ipc_connect(pid_t pid, ipc_connection_mode_E mode) {
    ipc_connection_T    connection;
    connection.pid                  = pid;
    connection.mode                 = mode;
    status_t status = STATUS_RESOURCE_BUSY;
    while (status == STATUS_RESOURCE_BUSY) {
        status = asm_syscall(SYSCALL_IPC_CONNECT, (uint64_t)&connection.descriptor, pid, mode, 0);
    }

    return connection;
}

// Waits until there is an ipc_connect call to this process
ipc_connection_T ipc_conn_wait(ipc_connection_mode_E mode) {
    ipc_connection_T connection;
    connection.mode = mode;
    status_t status = STATUS_ERROR;
    while (IS_STATUS_ERROR(status)) {
        status = asm_syscall(SYSCALL_IPC_CONN_WAIT, (uint64_t)&connection.descriptor, (uint64_t)&connection.pid, mode, 0);
    }

    return connection;
}

ipc_connection_T ipc_conn_wait_pid(pid_t pid, ipc_connection_mode_E mode) {
    // TODO: this is an absolute mess and NEEDS to be replaced!
    ipc_connection_T connection = ipc_conn_wait(mode);
    while (connection.pid != pid) {
        ipc_send_string(&connection, "BUSY");   // not handled yet
        ipc_close(&connection);
        connection = ipc_conn_wait(mode);
    }
    return connection;
}

status_t ipc_send_msg(ipc_connection_T* connection, ipc_message_T* msg) {
    return asm_syscall(SYSCALL_IPC_SEND, connection->descriptor, (uint64_t)msg, 0, 0);
}

status_t ipc_send(ipc_connection_T* connection, uint8_t* buffer, uint64_t len_buffer) {
    ipc_message_T msg = {0, IPC_MSG_MESSAGE, 0, (uint8_t*)buffer, len_buffer};
    return ipc_send_msg(connection, &msg);
}

status_t ipc_send_string(ipc_connection_T* connection, string_t str) {
    ipc_message_T msg = {0, IPC_MSG_MESSAGE, 0, (uint8_t*)str, strlen(str)};
    return ipc_send_msg(connection, &msg);
}

status_t ipc_send_signal(ipc_connection_T* connection, int signal) {
    ipc_message_T msg = {0, IPC_MSG_MESSAGE, 0, (uint8_t*)(uint64_t)signal, 0};
    return ipc_send_msg(connection, &msg);
}

ipc_message_T ipc_receive(ipc_connection_T* connection) {
    ipc_message_T   msg;
    status_t        status = STATUS_ERROR;

    while(IS_STATUS_ERROR(status)) {
        status = asm_syscall(SYSCALL_IPC_RECEIVE, connection->descriptor, (uint64_t)&msg, 0, 0);
    }

    return msg;
}


status_t ipc_close(ipc_connection_T* connection) {
    return asm_syscall(SYSCALL_IPC_CLOSE, connection->descriptor, 0, 0, 0);
}