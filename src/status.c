//
// Created by antifallobst on 10/27/22.
//
#include <cosmos/status.h>

string_t status_code_strings[NUM_STATUS_CODES] = {
        "Resource busy",
        "Timeout",
        "Resource N/A",
        "Permission denied",
        "Error (Unspecified)",
        "Warning (Unspecified)",
        "Success"
};

string_t status_get_string(status_t status) {
    return status_code_strings[status + NUM_STATUS_CODES];
}