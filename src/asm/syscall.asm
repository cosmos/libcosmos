asm_syscall:
    mov rax, rdi    ; ID
    mov rdi, rsi    ; ARG1
    mov rsi, rdx    ; ARG2
    mov rdx, rcx    ; ARG3
    mov rcx, r8     ; ARG4

    ; perform the syscall
    int 0x80

    ret
GLOBAL asm_syscall