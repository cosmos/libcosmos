//
// Created by antifallobst on 11/1/22.
//

#include <cosmos/module.h>
#include <cosmos/syscall.h>

pid_t mod_get_pid(module_E mod) {
    pid_t pid = 0;
    asm_syscall(SYSCALL_MOD_GET_PID, mod, (uint64_t)&pid, 0, 0);
    return pid;
}