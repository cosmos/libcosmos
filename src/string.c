//
// Created by antifallobst on 10/6/22.
//

#include <cosmos/string.h>
#include <cosmos/stdtypes.h>

int strlen(string_t str) {
    char*   chr = (char*)str;
    int     len =        0;

    while(chr[len] != 0) {
        ++len;
    }

    return len;
}

bool strcmp(string_t a, string_t b) {

    int size = strlen(a);
    if (size != strlen(b)) { return false; }

    for (int i = 0; i < size; i++) {
        if (*a != *b) return false;
        a++;
        b++;
    }
    return true;
}

void decimal_to_string(int64_t value, char* buffer) {
    if (buffer == NULL) {
        return;
    }
    uint8_t isNegative = 0;

    if (value < 0) {
        isNegative = 1;
        value *= -1;
        buffer[0] = '-';
    }

    uint8_t size;
    uint64_t sizeTest = value;
    while (sizeTest / 10 > 0) {
        sizeTest /= 10;
        size++;
    }

    uint8_t index = 0;
    while (value / 10 > 0) {
        uint8_t remainder = value % 10;
        value /= 10;
        buffer[isNegative + size - index] = remainder + '0';
        index++;
    }
    uint8_t remainder = value % 10;
    buffer[isNegative + size - index] = remainder + '0';
    buffer[isNegative + size + 1] = 0;
}