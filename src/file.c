//
// Created by antifallobst on 10/4/22.
//

#include <cosmos/file.h>
#include <cosmos/status.h>
#include <cosmos/syscall.h>

file_t      G_stdout    = {FILE_DESC_STDOUT, FILE_WRITE, 0};
file_t      G_stdlog    = {FILE_DESC_STDLOG, FILE_WRITE, 0};
file_t*       stdout    = &G_stdout;
file_t*       stdlog    = &G_stdlog;

file_t f_open(string_t path, file_mode_E mode) {
    file_t file;
    file.mode      = mode;
    file.cursor    = 0;

    status_t status = asm_syscall(SYSCALL_FS_OPEN, (uint64_t)path, strlen(path), mode, (uint64_t)&file.descriptor);

    if (status == STATUS_RESOURCE_NOT_AVAILABLE && mode == FILE_WRITE) {
//        asm_syscall(SYSCALL_FS_CREATE, (uint64_t)path, strlen(path), mode, (uint64_t)&file.descriptor);
    }

    return file;
}

status_t f_close(file_t* file) {
    return asm_syscall(SYSCALL_FS_CLOSE, file->descriptor, 0, 0, 0);
}

status_t f_delete(file_t* file) {
    return asm_syscall(SYSCALL_FS_DELETE, file->descriptor, 0, 0, 0);
}

status_t f_read(file_t* file, void* buffer, uint64_t num) {
    status_t status = asm_syscall(SYSCALL_FS_READ, file->descriptor, file->cursor, (uint64_t)buffer, num);
    if (status >= 0) {
        file->cursor += num;
    }
    return status;
}

status_t f_read_at_pos(file_t* file, void* buffer, uint64_t num, uint64_t pos) {
    return asm_syscall(SYSCALL_FS_READ, file->descriptor, pos, (uint64_t)buffer, num);
}

status_t f_write(file_t* file, void* buffer, uint64_t num) {
    status_t status = asm_syscall(SYSCALL_FS_WRITE, file->descriptor, file->cursor, (uint64_t)buffer, num);
    if (status >= 0) {
        file->cursor += num;
    }
    return status;
}

status_t f_write_at_pos(file_t* file, void* buffer, uint64_t num, uint64_t pos) {
    return asm_syscall(SYSCALL_FS_WRITE, file->descriptor, pos, (uint64_t)buffer, num);
}